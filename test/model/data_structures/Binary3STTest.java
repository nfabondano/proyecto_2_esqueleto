package model.data_structures;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import java.lang.reflect.Method;

import org.junit.jupiter.api.Test;

class Binary3STTest {
	
	//---------------------------Constantes-----------------------------------//
	
	/**
	 * Constante que ayuda a generar las llaves
	 */
	private final static String[] a1 = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","w","v","x","y","z"};

	/**
	 * Constante que ayuda a generar las llaves
	 */
	private final static String[] a2 = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","w","v","x","y","z"};
	
	
	//----------------------------Atributos-----------------------------------//
	
	/**
	 * Arbol que tiene como llaves numeros
	 */
	private Binary3ST<Integer,String> arbolDeNumeros;
	
	/**
	 * Arbol que tiene como llaves strings
	 */
	private Binary3ST<String, Integer> arbolDeString;
	
	/**
	 * arreglo dinamico donde se guardan las llaves
	 */
	private OrderedResizingArrayList<String> llaves;

	/**
	 * arreglo dinamico donde se guardan los valores
	 */
	private OrderedResizingArrayList<Integer> valores;
	
	//----------------------------M�todos-------------------------------------//
	
	
	/**
	 * 
	 */
	public void setUpEscenario0() {
		arbolDeNumeros = new Binary3ST<Integer,String>();
		arbolDeString = new Binary3ST<String, Integer>();
		llaves = new OrderedResizingArrayList<String>(100);
		valores = new OrderedResizingArrayList<Integer>(100);
	}
	/**
	 * 
	 */
	public void setUpEscenario1() {
		setUpEscenario0();
		for(int i = 0; i < a1.length; i++) {
			int x = (int)(Math.random()*((1000-0)+1))+0;
			for(int j = 0; j < a2.length; j++) {
				//System.out.println((a1[i] + a2[i]) + "");
				arbolDeString.insert(a1[i]+a2[j] + "", x);
				llaves.add(a1[i]+a2[j] + "");
				valores.add(x);
			}
		}
	}
	
	/**
	 * 
	 */
	public void setUpEscenario2() {
		setUpEscenario0();
		int length = 86686+74986+104219+107656 +108944+116722;
		for(int i = 0; i < length; i++) {
			arbolDeNumeros.insert(i,"lel");
		}
	}
	 
	/**
	 * M�todo con el que se rectifica que el arbol creado este balanceado
	 */
	@Test
	public void isBalanced0() {
		setUpEscenario0();
		Class c = arbolDeNumeros.getClass();
		Method privateMethod = null;
		boolean answer = true;
		try {
			privateMethod = c.getDeclaredMethod("isBalanced");
			System.out.println("pasa por aqui");
		}catch(Exception e) {
			e.printStackTrace();
		}
		privateMethod.setAccessible(true);
		try {
			 answer = (boolean) privateMethod.invoke(arbolDeNumeros);
			 System.out.println("Tambi�n pasa por aqu�");
		}catch(Exception e) {
			e.printStackTrace();
		}
		assertTrue("el arbol no esta bien balanceado", answer);
	}
	
	/**
	 * M�todo que rectifica que el arbol tenga la cantidad de elementos correctos
	 */
	@Test
	public void getSizeTest0() {
		setUpEscenario0();
		assertEquals("El arbol no deberia tener tama�o", arbolDeNumeros.getSize(), 0);
	}
	
	/**
	 * M�todo que rectifica que el arbol este vacio cuando se crea.
	 */
	@Test
	public void isEmptyTest0() {
		setUpEscenario0();
		assertTrue("El arbol no esta vacio",arbolDeNumeros.isEmpty());
	}
	
	/**
	 * M�todo que rectifica que se esten insertando elementos al arbol.
	 */
	@Test
	public void insertTest0() {
		setUpEscenario0();
		arbolDeNumeros.insert(1, "a");
		assertEquals("El tama�o del arbol debio haber aumentado", arbolDeNumeros.getSize(),1);
		arbolDeNumeros.insert(2, "a");
		assertEquals("El tama�o del arbol debio haber aumentado", arbolDeNumeros.getSize(),2);
		arbolDeNumeros.insert(2, "b");
		assertEquals("El tama�o del arbol debio haber aumentado", arbolDeNumeros.getSize(),2);
	}
	
	/**
	 * M�todo que prueba que se esten recuperando los valores del arbol de manera correcta
	 * seg�n la llave
	 */
	@Test
	public void getTest0() {
		setUpEscenario0();
		arbolDeNumeros.insert(1, "a");
		assertEquals("el elemento que se devolvio no es el correcto","a",arbolDeNumeros.get(1));
		arbolDeNumeros.insert(2, "b");
		assertEquals("el elemento que se devolvio no es el correcto","b",arbolDeNumeros.get(2));
		arbolDeNumeros.insert(2, "c");
		assertEquals("el elemento que se devolvio no es el correcto","c",arbolDeNumeros.get(2));
	}
	
	@Test
	public void isEmptyTest1() {
		setUpEscenario1();
		assertFalse("el arbol no deberia esta vacio",arbolDeString.isEmpty());
	}
	
	@Test
	public void getSizeTest1() {
		setUpEscenario1();
		int a = llaves.getSize();
		assertEquals("el tama�o del arbol no es el correcto", a, arbolDeString.getSize());
	}
	/**
	 * M�todo con el que se rectifica que el arbol creado este balanceado
	 */
	@Test
	public void isBalanced1() {
		setUpEscenario1();
		Class c = arbolDeString.getClass();
		Method privateMethod = null;
		boolean answer = true;
		try {
			privateMethod = c.getDeclaredMethod("isBalanced");
			System.out.println("pasa por aqui");
		}catch(Exception e) {
			e.printStackTrace();
		}
		privateMethod.setAccessible(true);
		try {
			 answer = (boolean) privateMethod.invoke(arbolDeString);
			 System.out.println("Tambi�n pasa por aqu�");
		}catch(Exception e) {
			e.printStackTrace();
		}
		assertTrue("el arbol no esta bien balanceado", answer);
	}
	
	/**
	 * M�todo que rectifica que los valores que se devuelven con el get corresponden a los insertados
	 */
	@Test
	public void getTest1() {
		setUpEscenario1();
		for(int i = 0; i < llaves.getSize(); i++) {
			assertEquals("el valor de la llave no corresponde con el valor con el que se creo", valores.getElement(i),arbolDeString.get(llaves.getElement(i)));
		}
	}
	
	@Test
	public void keysInRangeTest0() {
		setUpEscenario2();
		int length = 86686+74986+104219+107656 +108944+116722;
		//System.out.println(length);
		long start_time = System.currentTimeMillis();
		OrderedResizingArrayList<Integer> temp = arbolDeNumeros.keysInRange(0, length ,true);
		long end_time = System.currentTimeMillis();
		long difference = end_time-start_time;
		System.out.println(difference);
		int comp1 = 0;
		//System.out.println(temp.getElement(temp.getSize() -1));
		for(int e = 0; e < length; e++) {
			int comp2 = temp.getElement(e);
			assertEquals("El elemento regresado por el rango no es el correcto " + e + " " +comp1 + " " + comp2,comp1,comp2);
			comp1++;
		}
	}
	
	@Test
	public void keysInRangeTest1() {
		setUpEscenario2();
		int length = 86686+74986+104219+107656 +108944+116722;
		long start_time = System.currentTimeMillis();
		OrderedResizingArrayList<Integer> temp = arbolDeNumeros.keysInRange(0, length, false);
		long end_time = System.currentTimeMillis();
		long difference = end_time-start_time;
		System.out.println(difference);
		int comp1 = length-1;
		//System.out.println("el primer elemento es: " + temp.getElement(1));
		for(int e = 0; e < length; e++) {
			int comp2 = temp.getElement(e);
			assertEquals("El elemento regresado por el rango no es el correcto",comp1,comp2);
			comp1--;
		}
	}

}
