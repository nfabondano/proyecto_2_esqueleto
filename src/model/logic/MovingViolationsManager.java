package model.logic;
import java.io.File;
import java.io.FileReader;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.IdentityHashMap;
import java.util.Iterator;

import com.opencsv.CSVReader;

import model.data_structures.Binary3ST;
import model.data_structures.HashTableLP;
import model.data_structures.HashTableSC;
import model.data_structures.IQueue;
import model.data_structures.ITablaHash;
import model.data_structures.MaxHeapCP;
import model.data_structures.OrderedResizingArrayList;
import model.data_structures.Queue;
import model.vo.Coordinates;
import model.vo.EstadisticaInfracciones;
import model.vo.EstadisticasCargaInfracciones;
import model.vo.InfraccionesFecha;
import model.vo.InfraccionesFechaHora;
import model.vo.InfraccionesFranjaHoraria;
import model.vo.InfraccionesFranjaHorariaViolationCode;
import model.vo.InfraccionesLocalizacion;
import model.vo.InfraccionesViolationCode;
import model.vo.VOMovingViolations;

public class MovingViolationsManager {

	/**
	 * Clase auxiliar de fecha comparable
	 */
	private static class Fecha implements Comparable<Fecha>{
		private LocalDate fecha;
		public Fecha(LocalDate pFecha){
			fecha = pFecha;
		}
		public int compareTo(Fecha o) {
			if(fecha.isAfter(o.fecha)) 
				return 1;
			if(fecha.isBefore(o.fecha))
				return -1;
			return 0;
		}

	}

	/**
	 * Constante del archivo de enero.
	 */
	public final static String ENERO = "./data/Moving_Violations_Issued_In_January_2018.csv";

	/**
	 * Constante del archivo de febrero.
	 */
	public final static String FEBRERO = "./data/Moving_Violations_Issued_In_February_2018.csv";

	/**
	 * Constante del archivo de marzo.
	 */
	public final static String MARZO = "./data/Moving_Violations_Issued_In_March_2018.csv";

	/**
	 * Constante del archivo de abril.
	 */
	public final static String ABRIL = "./data/Moving_Violations_Issued_In_April_2018.csv";

	/**
	 * Constante del archivo de mayo.
	 */
	public final static String MAYO = "./data/Moving_Violations_Issued_In_May_2018.csv";

	/**
	 * Constante del archivo de junio.
	 */
	public final static String JUNIO = "./data/Moving_Violations_Issued_In_June_2018.csv";

	/**
	 * Constante del archivo de julio.
	 */
	public final static String JULIO = "./data/Moving_Violations_Issued_In_July_2018.csv";

	/**
	 * Constante del archivo de agosto.
	 */
	public final static String AGOSTO = "./data/Moving_Violations_Issued_In_August_2018.csv";

	/**
	 * Constante del archivo de septiembre.
	 */
	public final static String SEPTIEMBRE = "./data/Moving_Violations_Issued_In_September_2018.csv";

	/**
	 * Constante del archivo de octubre.
	 */
	public final static String OCTUBRE = "./data/Moving_Violations_Issued_In_October_2018.csv";

	/**
	 * Constante del archivo de noviembre.
	 */
	public final static String NOVIEMBRE = "./data/Moving_Violations_Issued_In_November_2018.csv";

	/**
	 * Constante del archivo de diciembre.
	 */
	public final static String DICIEMBRE = "./data/Moving_Violations_Issued_In_December_2018.csv";

	/**
	 * Archivos para leer en csv.
	 */
	private String[] csvData;

	private Binary3ST<LocalTime, Queue<VOMovingViolations>> FranjasHorarias;

	private Binary3ST<Fecha, Binary3ST<LocalTime, Queue<VOMovingViolations>>> FranjasFechaHora;

	private ITablaHash<Coordinates, Queue<VOMovingViolations>> InfraccionesCoordenadas;

	private ITablaHash<Integer, Queue<VOMovingViolations>> InfraccionesDirecciones;

	private Binary3ST<String, MaxHeapCP<VOMovingViolations>> ViolationCodes;

	private Binary3ST<Coordinates, Queue<VOMovingViolations>> ArbolLocalizacion;

	private Binary3ST<Fecha, IQueue<VOMovingViolations>> ArbolFechas;

	private int infraccionesTotales;
	/**
	 * Metodo constructor
	 */
	public MovingViolationsManager()
	{
		csvData = new String[12];
		csvData[0] = ENERO; 	csvData[4] = MAYO; 		csvData[8] = SEPTIEMBRE;
		csvData[1] = FEBRERO; 	csvData[5] = JUNIO; 	csvData[9] = OCTUBRE;
		csvData[2] = MARZO; 	csvData[6] = JULIO; 	csvData[10] = NOVIEMBRE;
		csvData[3] = ABRIL; 	csvData[7] = AGOSTO; 	csvData[11] = DICIEMBRE;

	}

	/**
	 * Cargar las infracciones de un semestre de 2018
	 * @param numeroSemestre numero del semestre a cargar (1 o 2)
	 * @return objeto con el resultado de la carga de las infracciones
	 */
	public EstadisticasCargaInfracciones loadMovingViolations(int numeroSemestre) {
		EstadisticasCargaInfracciones estadisticas = null;
		Coordinates maxLocation = new Coordinates(-5000000, -5000000);
		Coordinates minLocation = new Coordinates(5000000, 5000000);

		FranjasHorarias = new Binary3ST<LocalTime,Queue<VOMovingViolations>>();
		FranjasFechaHora = new Binary3ST<Fecha, Binary3ST<LocalTime,Queue<VOMovingViolations>>>();
		ViolationCodes = new Binary3ST<String, MaxHeapCP<VOMovingViolations>>();
		InfraccionesCoordenadas = new HashTableSC<Coordinates,Queue<VOMovingViolations>>(20000);
		InfraccionesDirecciones = new HashTableSC<Integer, Queue<VOMovingViolations>>(20000);
		ArbolLocalizacion = new Binary3ST<Coordinates, Queue<VOMovingViolations>>();
		ArbolFechas = new Binary3ST<Fecha,IQueue<VOMovingViolations>>();
		long tiempoFranjasHo = 0;
		long tiempoFranjasFech = 0;
		long tiempoViolationCodes = 0;
		long tiempoCoordenadas = 0;
		long tiempoDirecciones = 0;
		long tiempoArbolFechas = 0;
		int counter = 0;
		//long tiempoCarga = System.currentTimeMillis();
		try {
			int[] meses = new int[6];
			for(int i = 0; i<6; i++) {
				int mes = i+(numeroSemestre-1)*6;
				CSVReader reader = new CSVReader( new FileReader(new File(csvData[mes])));
				String nextLine[];
				nextLine = reader.readNext();
				meses[i] = 0;
				while((nextLine = reader.readNext()) != null) {
					//long totalStart = System.currentTimeMillis();
					VOMovingViolations a�adir = new VOMovingViolations(nextLine);
					LocalDateTime fecha = a�adir.getFecha();

					LocalTime franja = fecha.toLocalTime();
					franja = franja.truncatedTo(ChronoUnit.HOURS);

					long startTime = System.currentTimeMillis();
					if(FranjasHorarias.get(franja) == null) {
						Queue<VOMovingViolations> agregar = new Queue<VOMovingViolations>();
						agregar.enqueue(a�adir);
						FranjasHorarias.insert(franja, agregar);
					}
					else {
						Queue<VOMovingViolations> agregar = FranjasHorarias.get(franja);
						agregar.enqueue(a�adir);
						FranjasHorarias.insert(franja, agregar);
					}
					long endTime = System.currentTimeMillis();
					long duration = endTime - startTime;
					tiempoFranjasHo += duration;
					//System.out.println("La franja horaria toma: " + duration);
					LocalDate fechatemp = fecha.toLocalDate();
					Fecha date = new Fecha(fechatemp);			

					Binary3ST<LocalTime, Queue<VOMovingViolations>> arbolHoras = FranjasFechaHora.get(date);
					Queue<VOMovingViolations> agregar = new Queue<VOMovingViolations>();
					startTime = System.currentTimeMillis();
					if(arbolHoras == null) {
						arbolHoras = new Binary3ST<LocalTime, Queue<VOMovingViolations>>();	
					}
					else {
						agregar = arbolHoras.get(franja);
						if(agregar == null) 
							agregar = new Queue<VOMovingViolations>();
					}
					agregar.enqueue(a�adir);
					arbolHoras.insert(franja, agregar);
					FranjasFechaHora.insert(date, arbolHoras);
					endTime = System.currentTimeMillis();
					duration = endTime - startTime;
					tiempoFranjasFech += duration;
					//System.out.println("el arbol horas y franjaFEcha hora: " + duration);

					String code = a�adir.getViolationCode();
					startTime = System.currentTimeMillis();
					if(ArbolFechas.get(date) == null) {
						Queue<VOMovingViolations> infra = new Queue<VOMovingViolations>();
						infra.enqueue(a�adir);
						ArbolFechas.insert(date, infra);
					}
					else {
						Queue<VOMovingViolations> infra = (Queue<VOMovingViolations>)ArbolFechas.get(date);
						infra.enqueue(a�adir);
						ArbolFechas.insert(date, infra);
					}		
					endTime = System.currentTimeMillis();
					duration = endTime - startTime;
					tiempoArbolFechas += duration;
					//System.out.println("el arbol fechas: " + duration);

					startTime = System.currentTimeMillis();
					MaxHeapCP<VOMovingViolations> colaViolation = ViolationCodes.get(code);
					if(colaViolation == null) {
						colaViolation = new MaxHeapCP<VOMovingViolations>(19207);
					}
					colaViolation.enqueue(a�adir);
					ViolationCodes.insert(code, colaViolation);
					endTime = System.currentTimeMillis();
					duration = endTime - startTime;
					tiempoViolationCodes += duration;
					//System.out.println("el arbol ViolationCodes: " + duration);

					startTime = System.currentTimeMillis();
					Coordinates coordenadas = new Coordinates(a�adir.getXCOORD(), a�adir.getYCOORD());
					if(InfraccionesCoordenadas.contains(coordenadas)) {
						Queue<VOMovingViolations> porCoordenadas = InfraccionesCoordenadas.get(coordenadas);
						porCoordenadas.enqueue(a�adir);
						InfraccionesCoordenadas.put(coordenadas, porCoordenadas);
						ArbolLocalizacion.insert(coordenadas, porCoordenadas);
						//System.out.println("lul");
					}
					else {
						Queue<VOMovingViolations> porCoordenadas = new Queue<VOMovingViolations>();
						porCoordenadas.enqueue(a�adir);
						InfraccionesCoordenadas.put(coordenadas, porCoordenadas);
						ArbolLocalizacion.insert(coordenadas, porCoordenadas);
						//System.out.println("llega aqui");,399257,7 133847,29
					}
					endTime = System.currentTimeMillis();
					duration = endTime - startTime;
					tiempoCoordenadas += duration;
					//System.out.println("la tabla de coordenadas y el arbol de coordenadas: " + duration);
					startTime = System.currentTimeMillis();
					int address = a�adir.getAdressID(); Queue<VOMovingViolations> direcciones;
					if(InfraccionesDirecciones.contains(address)) {
						direcciones = InfraccionesDirecciones.get(address);
					}
					else {
						direcciones = new Queue<VOMovingViolations>();
					}
					direcciones.enqueue(a�adir);
					InfraccionesDirecciones.put(address, direcciones);
					endTime = System.currentTimeMillis();
					duration = endTime - startTime;
					tiempoDirecciones += duration;
					//System.out.println("Infracciones Direcciones : " + (duration));

					if(coordenadas.first > 0 && coordenadas.second > 0) {
						if(comparePairLocation(coordenadas, maxLocation) ==  1) maxLocation = coordenadas;
						if(comparePairLocation(coordenadas, minLocation) == -1) minLocation = coordenadas;
					}

					meses[i]++;
					//System.out.println(counter);
					counter++;
					//long totalEnd = System.currentTimeMillis();
					//duration = totalEnd - totalStart;
					//System.out.println("El tiempo total es: " + (duration));
				}	
				reader.close();
			}

			double[] coordenadas = new double[4];
			coordenadas[0] = minLocation.first; coordenadas[1] = minLocation.second;
			coordenadas[2] = maxLocation.first; coordenadas[3] = maxLocation.second;
			estadisticas = new EstadisticasCargaInfracciones(meses,coordenadas);
			infraccionesTotales = estadisticas.darTotalInfracciones();
			tiempoArbolFechas = tiempoArbolFechas / counter;
			tiempoCoordenadas = tiempoCoordenadas / counter;
			tiempoDirecciones = tiempoDirecciones / counter;
			tiempoFranjasFech = tiempoFranjasFech / counter;
			tiempoFranjasHo = tiempoFranjasHo / counter;
			tiempoViolationCodes = tiempoViolationCodes / counter;
			/*System.out.println("el promedio de tiempos es:\n tiempoArbolFechas : "+ tiempoArbolFechas
					+ "\n tiempoCoordenadas: " +tiempoCoordenadas + "\n tiempoDirecciones: " +
					tiempoDirecciones + "\n tiempoFranjasFech: " + tiempoFranjasFech + 
					"\n tiempoFranjasHo: " + tiempoFranjasHo + "\n tiempoViolationCodes: " + 
					tiempoViolationCodes);*/
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*long tiempoCargaFinal = System.currentTimeMillis();
		long difference = tiempoCargaFinal - tiempoCarga;
		System.out.println("tiempo en milisegundos: " + difference);
		*/
		return estadisticas;
	}

	/**
	 * Requerimiento 1A: Obtener el ranking de las N franjas horarias
	 * que tengan m�s infracciones. 
	 * @param int N: N�mero de franjas horarias que tienen m�s infracciones
	 * @return Cola con objetos InfraccionesFranjaHoraria
	 */
	public IQueue<InfraccionesFranjaHoraria> rankingNFranjas(int N)
	{
		MaxHeapCP<InfraccionesFranjaHoraria> respuesta = new MaxHeapCP<InfraccionesFranjaHoraria>(25);
		LocalTime min = FranjasHorarias.min();
		LocalTime max = FranjasHorarias.max();
		OrderedResizingArrayList<LocalTime> llaves = FranjasHorarias.keysInRange(min, max, true);
		for(LocalTime fecha : llaves) {
			LocalTime nuevo = fecha.plusMinutes(59);
			Queue<VOMovingViolations> toQueue = FranjasHorarias.get(fecha);
			InfraccionesFranjaHoraria toAdd = new InfraccionesFranjaHoraria(fecha, nuevo,toQueue);
			respuesta.enqueue(toAdd);
		}
		return respuesta.getNmaximums(N);
	}

	/**
	 * Requerimiento 2A: Consultar  las  infracciones  por
	 * Localizaci�n  Geogr�fica  (Xcoord, Ycoord) en Tabla Hash.
	 * @param  double xCoord : Coordenada X de la localizacion de la infracci�n
	 *			double yCoord : Coordenada Y de la localizacion de la infracci�n
	 * @return Objeto InfraccionesLocalizacion
	 */
	public InfraccionesLocalizacion consultarPorLocalizacionHash(double xCoord, double yCoord)
	{
		InfraccionesLocalizacion answer;
		Coordinates coordenadas = new Coordinates(xCoord,yCoord);
		Queue<VOMovingViolations> helper = InfraccionesCoordenadas.get(coordenadas);
		if(helper == null) {
			return null;
		}
		Iterator<VOMovingViolations> ite = helper.iterator();
		VOMovingViolations val = ite.next();
		VOMovingViolations max = val;
		while(ite.hasNext()) {
			if(val.getAdressID() != -1 && val.getStreetSgid() != -1) {
				answer = new InfraccionesLocalizacion(xCoord, yCoord,val.getLocation() ,val.getAdressID(),val.getStreetSgid(), helper);
				return answer;
			}
			val = ite.next();
		}
		answer = new InfraccionesLocalizacion(xCoord, yCoord,max.getLocation() ,max.getAdressID(), max.getStreetSgid(), helper);
		return answer;			
	}

	/**
	 * Requerimiento 3A: Buscar las infracciones por rango de fechas
	 * @param  LocalDate fechaInicial: Fecha inicial del rango de b�squeda
	 * 		LocalDate fechaFinal: Fecha final del rango de b�squeda
	 * @return Cola con objetos InfraccionesFecha
	 */
	public IQueue<InfraccionesFecha> consultarInfraccionesPorRangoFechas(LocalDate fechaInicial, LocalDate fechaFinal)
	{
		Fecha iniFech = new Fecha(fechaInicial);
		Fecha finFech = new Fecha(fechaFinal);
		OrderedResizingArrayList<Fecha> infrac = ArbolFechas.keysInRange(iniFech, finFech, true);
		Queue<InfraccionesFecha> answer = new Queue<InfraccionesFecha>();
		for(int i = 0; i < infrac.getSize(); i++) {
			Queue<VOMovingViolations> toAdd = (Queue<VOMovingViolations>)ArbolFechas.get(infrac.getElement(i));
			InfraccionesFecha nueva = new InfraccionesFecha(toAdd, infrac.getElement(i).fecha);
			answer.enqueue(nueva);
		}
		return answer;		
	}

	/**
	 * Requerimiento 1B: Obtener  el  ranking  de  las  N  tipos  de  infracci�n
	 * (ViolationCode)  que  tengan  m�s infracciones.
	 * @param  int N: Numero de los tipos de ViolationCode con m�s infracciones.
	 * @return Cola con objetos InfraccionesViolationCode con top N infracciones
	 */
	public IQueue<InfraccionesViolationCode> rankingNViolationCodes(int N)
	{
		MaxHeapCP<InfraccionesViolationCode> cola  = new MaxHeapCP<InfraccionesViolationCode>(150);
		Iterator<String> it = ViolationCodes.keys();
		while(it.hasNext()) {
			String violation = it.next();
			InfraccionesViolationCode est = new InfraccionesViolationCode(violation,ViolationCodes.get(violation));
			cola.enqueue(est);
		}
		return cola.getNmaximums(N);		
	}

	/**
	 * Requerimiento 2B: Consultar las  infracciones  por  
	 * Localizaci�n  Geogr�fica  (Xcoord, Ycoord) en Arbol.
	 * @param  double xCoord : Coordenada X de la localizacion de la infracci�n
	 *			double yCoord : Coordenada Y de la localizacion de la infracci�n
	 * @return Objeto InfraccionesLocalizacion
	 */
	public InfraccionesLocalizacion consultarPorLocalizacionArbol(double xCoord, double yCoord)
	{
		Coordinates cord = new Coordinates(xCoord, yCoord);
		Queue<VOMovingViolations> lista= ArbolLocalizacion.get(cord);
		String locat = null; int address = -1; int street = -1;
		if(lista != null) {
			VOMovingViolations elem = lista.dequeue();lista.enqueue(elem);
			locat = elem.getLocation(); address = elem.getAdressID(); street = elem.getStreetSgid();
		}
		InfraccionesLocalizacion est = new InfraccionesLocalizacion(xCoord, yCoord, locat, address, street, lista);
		return est;
	}

	/**
	 * Requerimiento 3B: Buscar las franjas de fecha-hora donde se tiene un valor acumulado
	 * de infracciones en un rango dado [US$ valor inicial, US$ valor final]. 
	 * @param  double valorInicial: Valor m�nimo acumulado de las infracciones
	 * 		double valorFinal: Valor m�ximo acumulado de las infracciones.
	 * @return Cola con objetos InfraccionesFechaHora
	 */
	public IQueue<InfraccionesFechaHora> consultarFranjasAcumuladoEnRango(double valorInicial, double valorFinal)
	{
		Iterator<Fecha> it = FranjasFechaHora.keys();
		MaxHeapCP<InfraccionesFechaHora> cola = new MaxHeapCP<InfraccionesFechaHora>(9207);
		while(it.hasNext()) {
			Fecha date = it.next();
			Binary3ST<LocalTime, Queue<VOMovingViolations>> arbolTemp = FranjasFechaHora.get(date);

			Iterator<LocalTime>ith = arbolTemp.keys();
			while(ith.hasNext()) {
				LocalTime time1 = ith.next();
				LocalTime time2 = time1.plusHours(1).minusNanos(10000000);
				LocalDateTime date1 = date.fecha.atTime(time1);
				LocalDateTime date2 = date.fecha.atTime(time2);

				Queue<VOMovingViolations> lista = arbolTemp.get(time1);
				InfraccionesFechaHora est = new InfraccionesFechaHora(date1, date2, lista);

				double valor = est.getValorTotal();

				if(valor <= valorFinal  && valor >= valorInicial) {
					cola.enqueue(est);
				}

			}
		}
		return cola;		
	}

	/**
	 * Requerimiento 1C: Obtener  la informaci�n de  una  addressId dada
	 * @param  int addressID: Localizaci�n de la consulta.
	 * @return Objeto InfraccionesLocalizacion
	 */
	public InfraccionesLocalizacion consultarPorAddressId(int addressID)
	{
		Queue<VOMovingViolations> lista = InfraccionesDirecciones.get(addressID);
		double xcoor = -1; double ycoor = -1; String locat = null; int address = -1; int street = -1;
		if(lista != null) {
			VOMovingViolations elem = lista.dequeue();lista.enqueue(elem);
			xcoor = elem.getXCOORD(); ycoor = elem.getYCOORD();
			locat = elem.getLocation(); address = elem.getAdressID(); street = elem.getStreetSgid();
		}
		return new InfraccionesLocalizacion(xcoor, ycoor, locat, address, street, lista);		
	}

	/**
	 * Requerimiento 2C: Obtener  las infracciones  en  un  rango de
	 * horas  [HH:MM:SS  inicial,HH:MM:SS  final]
	 * @param  LocalTime horaInicial: Hora  inicial del rango de b�squeda
	 * 		LocalTime horaFinal: Hora final del rango de b�squeda
	 * @return Objeto InfraccionesFranjaHorariaViolationCode
	 */
	public InfraccionesFranjaHorariaViolationCode consultarPorRangoHoras(LocalTime horaInicial, LocalTime horaFinal)
	{
		Iterator<String> it = ViolationCodes.keys();
		MaxHeapCP<VOMovingViolations> cola;
		Queue<InfraccionesViolationCode> violationEst = new Queue<InfraccionesViolationCode>();
		InfraccionesFranjaHorariaViolationCode est = new InfraccionesFranjaHorariaViolationCode(horaInicial, horaFinal, null, null);
		while(it.hasNext()) {
			String code = it.next();
			cola = ViolationCodes.get(code);
			Queue<VOMovingViolations> nuevaCola = new Queue<VOMovingViolations>();
			for(VOMovingViolations infraccion : cola) {
				if(infraccion.getFecha().toLocalTime().compareTo(horaFinal)<=0 && infraccion.getFecha().toLocalTime().compareTo(horaInicial)>=0)
					nuevaCola.enqueue(infraccion);
				else
					break;
			}
			if(nuevaCola.size() != 0) {
				est.a�adirListaInfracciones(nuevaCola);
				InfraccionesViolationCode infra = new InfraccionesViolationCode(code, nuevaCola);
				violationEst.enqueue(infra);
			}
		}
		est.setInfViolationCode(violationEst);
		return est;	
	}

	/**
	 * Requerimiento 3C: Obtener  el  ranking  de  las  N localizaciones geogr�ficas
	 * (Xcoord,  Ycoord)  con  la mayor  cantidad  de  infracciones.
	 * @param  int N: Numero de las localizaciones con mayor n�mero de infracciones
	 * @return Cola de objetos InfraccionesLocalizacion
	 */
	public IQueue<InfraccionesLocalizacion> rankingNLocalizaciones(int N)
	{
		/*Coordinates min = ArbolLocalizacion.min();
		Coordinates max = ArbolLocalizacion.max();
		OrderedResizingArrayList<Coordinates> llaves = ArbolLocalizacion.keysInRange(min, max, true);*/
		Iterator<Coordinates> llaves = ArbolLocalizacion.keys();
		MaxHeapCP<InfraccionesLocalizacion> answer = new MaxHeapCP<InfraccionesLocalizacion>(599307);
		
		while(llaves.hasNext()) {
			Coordinates coor = llaves.next();
			
			Queue<VOMovingViolations> cola = ArbolLocalizacion.get(coor);
			VOMovingViolations example =  cola.first();
			InfraccionesLocalizacion toAdd = new InfraccionesLocalizacion(coor.first, coor.second,example.getLocation() , example.getAdressID(), example.getStreetSgid(), cola);
			answer.enqueue(toAdd);
		}
		return answer.getNmaximums(N);		
	}

	/**
	 * Requerimiento 4C: Obtener la  informaci�n  de  los c�digos (ViolationCode) ordenados por su numero de infracciones.
	 * @return Contenedora de objetos InfraccionesViolationCode.
	  // TODO Definir la estructura Contenedora
	 */
	public MaxHeapCP<InfraccionesViolationCode> ordenarCodigosPorNumeroInfracciones()
	{
		Iterator<String> codigos = ViolationCodes.keys();
		MaxHeapCP<InfraccionesViolationCode> answer = new MaxHeapCP<InfraccionesViolationCode>(207);
		while(codigos.hasNext()) {
			String llave = codigos.next();
			MaxHeapCP<VOMovingViolations> data = ViolationCodes.get(llave);
			InfraccionesViolationCode toAdd = new InfraccionesViolationCode(llave, data);
			answer.enqueue(toAdd);
		}
		return answer;		
	}

	public int getTotalViolations() {
		return infraccionesTotales;
	}

	/**
	 * Metodo auxiliar para la comparaci�n de coordenadas
	 * @param p1 Coordenada 1
	 * @param p2 Coordenada 2
	 * @return Cu�l es mayor
	 */
	public int comparePairLocation(Coordinates p1, Coordinates p2) {
		Double x1 = p1.first; 	Double x2 = p2.first;
		Double y1 = p1.second; 	Double y2 = p2.second;

		if(x1> x2) return 1;
		else if(x1 < x2) return -1;
		else if(y1 > y2) return 1;
		else if(y1 < y2) return -1;
		else return 0;
	}

}
