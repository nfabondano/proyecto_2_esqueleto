package model.data_structures;

import java.util.Comparator;
import java.util.Iterator;

import model.data_structures.OrderedResizingArrayList.IntComparator;

public class Binary3ST<Key extends Comparable<Key>, Value>{
	
	//-------------------------------Constantes------------------------------------//
	
	/**
	 * Constante que representa el color negro
	 */
	private final static boolean BLACK = false;
	
	/**
	 * Constante que representa el color rojo
	 */
	private final static boolean RED = true;

	
	//--------------------------------Atributos------------------------------------//
	
	/**
	 * Nodo que se ubica en la raiz del arbol
	 */
	private Node root;
	
	/**
	 * Arreglo de llaves que se devuelven cuando el usuario lo requiere
	 */
	private OrderedResizingArrayList<Key> keys;
	
	/**
	 * arreglo donde se guardan las llaves para entregar al usuario 
	 */
	private OrderedResizingArrayList<Value> values;

	/**
	 * Arreglo donde se almacenan todas las llaves que se adicionan al arbol.
	 */
	private OrderedResizingArrayList<Key> otherKeys;
	
	/**
	 * Constructor que inicializa las listas de llaves y valores.
	 */
	public Binary3ST() {
		keys = new OrderedResizingArrayList<Key>();
		values = new OrderedResizingArrayList<Value>();
		otherKeys = new OrderedResizingArrayList<Key>(399207);
	}
	
	//--------------------------------Clase Auxuliar------------------------------//
	
	/**
	 * Clase auxiliar que representa los nodos que se van a utilizar para construir el arbol
	 *
	 */
	class Node{
		//----------------------------Atributos-----------------------------------//
		
		/**
		 * La llave que identifica esa posici�n del arbol
		 */
		Key key;
		
		/**
		 * valor que va a guardar en el nodo
		 */
		Value val;
		
		/**
		 * los nodos (hijos) izquierdos y derechos
		 */
		Node right, left;
		
		/**
		 * numero de nodos "abajo" m�s el mismo
		 */
		int N;
		
		/**
		 * el color del nodo
		 */
		boolean color;
		
		//-------------------------------Constructor----------------------------//
		
		
		/**
		 * M�todo que inicializa el nodo con la llave que va a tener, la altura que tiene en el arbol el color y el valor que va a guardar
		 * @param pKey la llave que va a tener. PKey != null
		 * @param pVal el valor que va guardar
		 * @param pN la altura que va a tener. pN >= 0
		 * @param pColor el color que va a tener.
		 */
		public Node(Key pKey, Value pVal, int pN, boolean pColor){
			this.key = pKey; this.val = pVal;this.color = pColor; this.N = pN; this.right = null; this.left = null;
		}
	}
	
	/**
	 * M�todo que regresa falso o verdadero si el nodo es un nodo de tipo 2
	 * @param x el nodo que se quiere rectificar. 
	 * @return falso o verdadero si el nodo es de tipo 2
	 */
	private boolean is2Node(Node x){
		if(x == null){
			int a = 2 / 0;
		}
		if((x.right != null && x.right.color) || (x.left != null && x.left.color)) return false;
		if(x.color) return false;
		if(!x.color) return true;
		return (x.right == null && x.left == null);
	}

	/**
	 * M�todo que mueve havia la izquierda el nodo pasado por parametro 
	 * en relaci�n de su hijo derecho
	 * @param x nodo que se va a rotar a la izquierda. x != null
	 * @return nuevo nodo que queda en la posici�n de su padre.
	 * post: el arbol ha quedado balanceado localmente
	 */
	private Node rotateLeft(Node x){
		Node t = x.right;
		//t.left = x; THIS WAS THE ERROR
		//System.out.println("rotado a la izquierda " + x.key);
		x.right = t.left;
		t.left = x;
		t.color = x.color;
		x.color = RED;
		t.N = x.N;
		x.N = size(x.left) + size(x.right) + 1;
		return t;
	}
	
	/**
	 * M�todo que rota hacia la derecha el nodo actual con respecto al 
	 * hijo izquierdo
	 * @param x nodo que se va a rotar. x != null
	 * @return nuevo nodo que queda en la posici�n de su padre.
	 * post: el arbol ha quedado balanceado localmente
	 */
	private Node rotateRight(Node x){
		//System.out.println("right " + x.left.val + " " + x.left.left.val);
		//System.out.println("rotado a la derecha " + x.key);
		Node t = x.left;
		//t.right = x; THIS WAS THE ERROR!!!!! 
		x.left = t.right;
		t.right = x;
		t.color = x.color;
		x.color = RED;
		t.N = x.N;
		x.N = size(x.left) + size(x.right) + 1;
		return t;
	}
	
	/**
	 * M�todo que informa si el nodo pasado por parametro es rojo
	 * @param x nodo al cual se va a rectificar si es rojo
	 * @return true o false seg�n si el nodo es rojo
	 */
	private boolean isRed(Node x){
		if(x == null) return false;
		return x.color;
	}
	
	/**
	 * M�todo que cambia los colores del nodo pasado por parametro a rojo
	 * y los hijos los convierte a color negro
	 * @param x nodo al cual se le van a cambiar los colores y el de los hijos. x != null
	 * post: se han cambiado los colores del nodo pasado por paramtero y sus hijos
	 */
	private void changeColors(Node x){
		//System.out.println("called on " + x.key);
		x.color = RED;
		x.left.color = BLACK;
		x.right.color = BLACK;
	}
	
	/**
	 * Devuelve el n�mero de nodos en el arbol
	 * @return n�mero de nodos en el arbol.
	 */
	public int getSize(){
		return size(root);
	}
	
	/**
	 * M�todo que devuelve el n�mero de nodos bajo el nodo pasado por parametro m�s uno
	 * @param x el nodo del cual se va a obtener el dato. 
	 * @return el n�mero de nodos bajo el nodo pasado por parametro m�s uno
	 */
	private int size(Node x){
		if(x == null) return 0;
		return x.N;
	}

	/**
	 * M�todo que inserta un nodo en el arbol
	 * @param pKey la llave del nodo
	 * @param pVal el valor que se va a guardar en el nodo
	 * post: se ha insertado un nodo en el arbol y el arbol ha quedado balanceado
	 */
	public void insert(Key pKey, Value pVal){
		root = insert(root, pKey, pVal);
		root.color = BLACK;
	}

	/**
	 * M�todo que inserta un nuevo nodo en el subArbol que comienza en el nodo pasado por parametro,
	 * Pasando la referencia del nodo insertado a su padre
	 * @param x raiz del subArbol donde se piensa insertar el nuevo nodo
	 * @param pKey la llave del nuevo nodo
	 * @param pVal valor que va a tener el nuevo nodo
	 * @return devuelve la referencia del nodo insertado a su padre correspondiente
	 */
	private Node insert(Node x, Key pKey, Value pVal){
		if(x == null) { otherKeys.add(pKey); return new Node(pKey, pVal, 1, RED);}
		int cmp = pKey.compareTo(x.key);
		if(cmp < 0) x.left = insert(x.left, pKey, pVal);
		else if(cmp > 0) x.right = insert(x.right, pKey, pVal);
		else x.val = pVal;

		if(!isRed(x.left) && isRed(x.right)) x = rotateLeft(x);
		if(isRed(x.left) && isRed(x.left.left)) x = rotateRight(x);
		if(isRed(x.left) && isRed(x.right)) changeColors(x);
		x.N = size(x.left) + size(x.right) + 1;
		return x;
	}
	
	
	/**
	 * M�todo que covierta el nodo 
	 * @param x
	 */
	/*private void convertTo4(Node x){
		x.color = BLACK;
		x.left.color = RED;
		if(x.right != null) x.right.color = RED;
	}
	*/
	/**
	 * Devuelve el valor del nodo con llave pasada por parametro.
	 * @param pKey la llave del nodo del cual se quiere saber el valor guardado. pKey != null.
	 * @return el valor del nodo con llave pasada por parametro.
	 */
	public Value get(Key pKey){

		return get(root, pKey);
	}
	
	/**
	 * Busca el nodo con la llave pasada por parametro a partir del subArbol del nodo pasado por parametros
	 * @param x nodo que comienza el subarbol donde se va a buscar el nodo con llave especificada. 
	 * @param pKey la llave del nodo que se esta buscando. pKey != null
	 * @return el valor del nodo con la llave pasada por parametro o null si no se encuentra.
	 */
	private Value get(Node x, Key pKey){
		if(x == null) return null;
		int cmp = pKey.compareTo(x.key);
		if(cmp < 0) return get(x.left, pKey);
		if(cmp > 0) return get(x.right, pKey);
		return x.val;
	}
	
	/**
	 * M�todo que el valor minimo del arbol.
	 * post: se ha borrado el valor minimo del arbol y el arbol ha quedado balanceado.
	 */
	public void deleteMin(){
		System.out.println(root.val);
		root = changeRoot(root);
		System.out.println(root.val);
		root = deleteMin(root);
		System.out.println("to the left of the root is: " + root.left.key);
		root.color = BLACK;
	}
	
	
	/**
	 * M�todo que cambia la raiz del arbol de tal manera que sea un 3 nodo si no lo es. 
	 * @param x el nodo que representa la raiz.
	 * @return la nueva raiz convertida a un 3 nodo.
	 */
	private Node changeRoot(Node x){
		if(x == null) return null;
		if(!is2Node(x.left)) return x;
		if(is2Node(x.right)){
			x.left.color = RED;
			x.right.color = RED;
			return x;
		}
		Node t1 = x.right.left;
		Node t2 = t1.left;
		x.left.color = RED;
		x.right.left = t1.right;
		t1.right = x.right;
		x.right = t2;
		t1.left = x;
		t1.right.color = BLACK;
		t1.left.color = BLACK;
		return t1;

	}
	
	/**
	 * M�todo que borra el elemento minimo del subArbol que comienza en el nodo pasado por parametro.
	 * @param x nodo desde donde comienza el subArbol. x != null 
	 * @return la nueva referencia al nodo que se ubica a la izquierda del nodo pasado por parametro.
	 */
	private Node deleteMin(Node x){
		if(x == null) return null;
		//System.out.println(x.key);
		if(x.left == null) return null;
		if(!is2Node(x.left)) {
			x.left = deleteMin(x.left);
			if(!isRed(x.left) && isRed(x.right)) x = rotateLeft(x);
			if(isRed(x.left) && isRed(x.left.left)) x = rotateRight(x);
			if(isRed(x.left) && isRed(x.right)) changeColors(x);
			x.N = size(x.left) + size(x.right) + 1;
			return x;
		}

		if(is2Node(x.left) && !is2Node(x.right)){
			Node t1 = x.right.left;
			Node t2 = t1.left;
			x.left.color = RED;
			x.right.left = t1.right;
			t1.right = x.right;
			x.right = t2;
			t1.left = x;
			t1.color = x.color;
			t1.right.color = BLACK;
			t1.left.color = BLACK;
			t1.left = deleteMin(t1.left);
			if(!isRed(t1.left) && isRed(t1.right)) t1 = rotateLeft(t1);
			if(isRed(t1.left) && isRed(t1.left.left)) t1 = rotateRight(t1);
			if(isRed(t1.left) && isRed(t1.right)) changeColors(t1);
			t1.N = size(t1.left) + size(t1.right) + 1;
			return t1;
		}

		x.color = BLACK;
		x.left.color = RED;
		x.right.color = RED;
		x.left = deleteMin(x.left);
		if(!isRed(x.left) && isRed(x.right)) x = rotateLeft(x);
		if(isRed(x.left) && isRed(x.left.left)) x = rotateRight(x);
		if(isRed(x.left) && isRed(x.right)) changeColors(x);
		x.N = size(x.left) + size(x.right) + 1;
		return x;

	}
	
	/**
	 * M�todo que retonar verdadero o falso seg�n si el arbol esta vacio.
	 * @return verdadero o falso seg�n si el arbol esta vacio.
	 */
	public boolean isEmpty(){
		if(root == null) return true;
		return false;
	}
	
	/**
	 * M�todo que devuelve las llaves que se encuentran en un rango especifico
	 * @param lesstn minimo del rango. lesstn != null
	 * @param moretn maximo del rango. moretn != null
	 * @param ascending notifica al m�todo como se deben agregar las llaves.
	 * @return retorna las lalves en un rango especifico de manera ascendente o descendente.
	 */
	public OrderedResizingArrayList<Key> keysInRange(Key lesstn, Key moretn,boolean ascending){
		if (root == null)
			return null;
		keys = new OrderedResizingArrayList<Key>();
		values = new OrderedResizingArrayList<Value>();
		if(ascending)
			keysValuesInRangeMinimumOriented(root,lesstn,moretn,true);
		else
			keysValuesInRangeMaxOriented(root, lesstn, moretn,true);
		return keys;
	}
	
	/**
	 * M�todo que agrega las llaves/valores de manera descendente en el arreglo keys/values 
	 * @param start nodo desde el que comienza para determinar el camino en el arbol. start != null
	 * @param lesstn minimo del rango. lesstn != null
	 * @param moretn maximo del rango. moretn != null
	 * @param key notifica al m�todo si se estan agregando llaves o valores.
	 */
	private void keysValuesInRangeMaxOriented(Node start,Key lesstn, Key moretn, boolean key) {
		if(start == null)
			return;
		if(start.key.compareTo(lesstn) >= 0 && start.key.compareTo(moretn) <= 0) {
			if(key) {
				keysValuesInRangeMaxOriented(start.right,lesstn,moretn,key);
				keys.add(start.key);
				keysValuesInRangeMaxOriented(start.left, lesstn, moretn,key);
			}
			else {
				keysValuesInRangeMaxOriented(start.right,lesstn,moretn,key);
				values.add(start.val);
				keysValuesInRangeMaxOriented(start.left, lesstn, moretn,key);
			}
		}
		if(start.key.compareTo(moretn) > 0) 
			keysValuesInRangeMaxOriented(start.left,lesstn,moretn,key);
		if(start.key.compareTo(lesstn) < 0)
			keysValuesInRangeMaxOriented(start.right, lesstn, moretn,key);
	}
	
	/**
	 * M�todo que agrega las llaves/valores de manera ascendente en el arreglo keys/valores
	 * @param start nodo desde el que comienza para determinar el camino en el arbol. start != null
	 * @param lesstn minimo del rango. lesstn != null
	 * @param moretn maximo del rango. moretn != null
	 * @param key notifica al m�todo si se estan agregando llaves o valores.
	 */
	private void keysValuesInRangeMinimumOriented(Node start,Key lesstn, Key moretn, boolean key) {
		if(start == null)
			return;
		if(start.key.compareTo(lesstn) >= 0 && start.key.compareTo(moretn) <= 0) {
			if(key) {
				keysValuesInRangeMinimumOriented(start.left,lesstn,moretn,key);
				keys.add(start.key);
				keysValuesInRangeMinimumOriented(start.right, lesstn, moretn,key);
			}
			else {
				keysValuesInRangeMinimumOriented(start.left,lesstn,moretn,key);
				values.add(start.val);
				keysValuesInRangeMinimumOriented(start.right, lesstn, moretn,key);
			}
		}
		if(start.key.compareTo(moretn) > 0) 
			keysValuesInRangeMinimumOriented(start.left,lesstn,moretn,key);
		if(start.key.compareTo(lesstn) < 0)
			keysValuesInRangeMinimumOriented(start.right, lesstn, moretn,key);
	}
	
	/**
	 * M�todo que devuelve las llaves que se encuentran en un rango especifico
	 * @param lesstn minimo del rango. lesstn != null
	 * @param moretn maximo del rango. moretn != null
	 * @param ascending notifica al m�todo como debe agregar los valores.
	 * @return retorna las lalves en un rango especifico de manera ascendente o descendente.
	 */
	public OrderedResizingArrayList<Value> valuesInRange(Key lesstn, Key moretn,boolean ascending){
		if (root == null)
			return null;
		keys = new OrderedResizingArrayList<Key>();
		values = new OrderedResizingArrayList<Value>();
		if(ascending)
			keysValuesInRangeMinimumOriented(root,lesstn,moretn, false);
		else
			keysValuesInRangeMaxOriented(root, lesstn, moretn,false);
		return values;
	}
	
	/**
	 * M�todo que rectifca que el arbol este balanceado y devuelve la respuesta. 
	 * @return verdadero o falso seg�n si el arbol esta balanceado.
	 */
	private boolean isBalanced() {
		if(root == null)
			return true;
		return isBalanced(root)[1] == 1;
	}
	
	/**
	 * M�todo que cuenta la longitud del cada una de las ramas del subArbol que comienza
	 * en el nodo pasado por parametro y notifica su altura y si esta balanceado.
	 * @param start nodo desde el cual comienza el subArbol. start != null
	 * @return un arreglo donde se evidencia la altura del arbol y si esta balanceado
	 */
	private int[] isBalanced(Node start) {
		if(start.left == null && start.right == null) {
			if(start.color) {
				int answer[] = {0,1};
				return answer;
			}
			int answer[] = {1,1};
			return answer;
		}
		else if(start.left != null && start.right != null) {
			int left[] = isBalanced(start.left);
			int right[] = isBalanced(start.right);
			if(left[1] == 1 && right[1] == 1) {
				if(left[0] == right[0]) {
				//	System.out.println("Entra aca");
					int answer[] = {0,0};
					if(start.color) {
						answer[0] = (left[0]);
						answer[1] = 1;
					}
					else {
						answer[0] = (left[0])+1;
						answer[1] = 1;
					}
					return answer;
				}
			}
			//System.out.println("Aqui tiene que ser el error left es: " + left[0] + " " + right[0]);
			int answer[] = {0,0};
			return answer;
		}
		else if(start.left != null && start.right == null) {
			if(isBalanced(start.left)[0] == 0) {
				int answer[] = {1,1};
				return answer;
			}
			//System.out.println("tal vez aqui?  "+ isBalanced(start.left)[0]);
			int answer[] = {1,0};
			return answer;
		}
		//System.out.println("es posible que pase por aqui?");
		int answer[] = {0,0};
		return answer;
	}
	
	/*public OrderedResizingArrayList<Value> ValuesInRange(Key lesstn, Key moretn) {
		//Comparator<Key> order = (Comparator<Key>) new IntComparator();
		OrderedResizingArrayList<Value> answer = new OrderedResizingArrayList<Value>();
		//otherKeys.sort(order);
		int hi = otherKeys.getSize()-1;
		int lo = 0;
		int mid = lo + (hi - lo)/2;
		while(lo <= hi) {
			if(otherKeys.getElement(mid).compareTo(lesstn)>= 0 && (mid == 0 || otherKeys.getElement(mid-1).compareTo(lesstn) < 0))
				break;
			else if(otherKeys.getElement(mid).compareTo(lesstn) < 0)
				lo = mid + 1;
			else
				hi = mid - 1;
			mid = lo + (hi - lo)/2;
		}
		for(int i = mid; i < otherKeys.getSize(); i++) {
			if(otherKeys.getElement(i).compareTo(moretn) > 0)
				break;
			answer.add(get(otherKeys.getElement(i)));
		}
		return answer;
	}*/
	/**
	 * Retorna la llave m�s peque�a del �rbol. Valor null si �rbol vac�o
	 */
	public Key min() {
		return min(root);
	}

	/**
	 * Retorna el valor m�nimo desde un nodo por par�metro
	 * @param n Nodo desde donde se desea buscar el minimo.
	 * @return El valor m�nimo desde el nodo
	 */
	private Key min(Node n) {
		if(n.left == null) return n.key;
		return min(n.left);
	}
	
	/**
	 * Retorna la llave m�s grande del �rbol. Valor null si �rbol vac�o
	 */
	public Key max() {
		return max(root);
	}

	/**
	 * Retorna el valor m�ximo desde un nodo por par�metro.
	 * @param n Nodo desde donde se desea buscar el m�ximo.
	 * @return El valor m�ximo desde el nodo.
	 */
	private Key max(Node n) {
		if(n.right == null) return n.key;
		return min(n.right);
	}
	
	public Iterator<Key> keys() {
		return otherKeys.iterator();
	}
	
	class IntComparator implements Comparator<Integer> {

	    @Override
	    public int compare(Integer v1, Integer v2) {
	        return v1 < v2 ? -1 : v1 > v2 ? +1 : 0;
	    }
	}
	
}