	package model.data_structures;

/**
 * Interface de la clase auxiliar Pair
 * @param <K> tipo de llave
 * @param <V> tipo de valor
 */
public interface IPair<K, V extends Comparable<V>> extends Comparable<IPair<K,V>>{
	
	/**
	 * M�todo que retorna la llave del pair.
	 * @return La llave del pair.
	 */
	public K getKey();
	
	/**
	 * M�todo que retorna el valor del pair.
	 * @return El valor del pair.
	 */
	public V getValue();
}
