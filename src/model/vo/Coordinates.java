package model.vo;

public class Coordinates implements Comparable<Coordinates> {
	/**
	 *
	 */
	public Double first;
	
	/**
	 * 
	 */
	public Double second;
	
	/**
	 * 
	 * @param pFirst
	 * @param pSecond
	 */
	public Coordinates(double pFirst, double pSecond) {
		first = pFirst;
		second = pSecond;
	}

	/**
	 * 
	 */
	public int compareTo(Coordinates arg0) {
		if(first.compareTo(arg0.first) > 0)
			return 1;
		if(first.compareTo(arg0.first) < 0)
			return -1;
		if(second.compareTo(arg0.second) > 0)
			return 1;
		if(second.compareTo(arg0.second) < 0)
			return -1;
		return 0;
	}
	public String toString() {
		return "coordenada x "+ first + "coordenda y" + second;
	}
	
	public int hashCode() {
		return first.hashCode();
	}
	
	public boolean equals(Coordinates obj) {
		if(first.equals(obj.first) && second.equals(obj.second))
			return true;
		return false;
	}
}
