package model.vo;

import model.data_structures.IQueue;
import model.data_structures.Queue;

/**
 * Agrupa las infracciones mostrando estadísticas sobre los datos 
 * como el total de infracciones que se presentan en ese conjunto,
 * el porcentaje de infracciones con y sin accidentes con respecto al total,
 * el valor total de las infracciones que se deben pagar y una lista con 
 * las infracciones. 
 */

public class EstadisticaInfracciones{

	@Override
	public String toString() {
		return "EstadisticaInfracciones [totalInfracciones=" + totalInfracciones + ",\n porcentajeAccidentes="
				+ porcentajeAccidentes + ",\n porcentajeNoAccidentes=" + porcentajeNoAccidentes + ",\n valorTotal="
				+ valorTotal + "]\n\n";
	}

	/**	
	 * Numero total de infraciones del conjunto
	 */

	protected int totalInfracciones;

	protected int conAccidentes;

	protected int sinAccidentes;

	/**
	 * Porcentaje de las infracciones con accidentes con respecto al total
	 */

	protected double porcentajeAccidentes;

	/**
	 * Porcentaje de las infracciones sin accidentes con respecto al total
	 */

	protected double porcentajeNoAccidentes; 

	/**
	 * Valor total de las infracciones que se debe pagar.
	 */

	protected double valorTotal;	

	/**
	 * Lista con las infracciones que agrupa el conjunto
	 */

	protected IQueue<VOMovingViolations> listaInfracciones;


	/**
	 * Crea un nuevo conjunto con las infracciones
	 * @param listaInfracciones - Lista con las infracciones que cumplen el criterio de agrupamiento
	 */

	public EstadisticaInfracciones(IQueue<VOMovingViolations> lista) {

		valorTotal = 0;
		porcentajeAccidentes = 0;
		porcentajeNoAccidentes = 0;
		conAccidentes = 0;
		sinAccidentes = 0;
		if(lista!=null) {
			this.listaInfracciones = lista;
			totalInfracciones = listaInfracciones.size();
			for(VOMovingViolations violations : lista){
				if(violations.getAccidentIndicator().equalsIgnoreCase("yes"))
					conAccidentes += 1;
				else
					sinAccidentes += 1;
				valorTotal += violations.getTotalPaid();
			}

			porcentajeAccidentes = (conAccidentes / (double)(lista.size()))*100;   //TODO Calcular con base en la lista
			porcentajeNoAccidentes = (sinAccidentes / (double)(lista.size()))*100; //TODO Calcular con base en la lista
		}
	}

	//=========================================================
	//Metodos Getters and Setters
	//=========================================================

	/**
	 * Gets the total infracciones.
	 * @return the total infracciones
	 */

	public int getTotalInfracciones() {
		return totalInfracciones;
	}	


	/**
	 * Gets the porcentaje accidentes.	 *
	 * @return the porcentaje accidentes
	 */

	public double getPorcentajeAccidentes() {
		//con respecto al total.
		return porcentajeAccidentes;
	}	


	/**
	 * Gets the porcentaje no accidentes.
	 *
	 * @return the porcentaje no accidentes
	 */
	public double getPorcentajeNoAccidentes() {
		//con respecto al total.
		return porcentajeNoAccidentes;
	}

	/**
	 * Gets the valor total.
	 *
	 * @return the valor total
	 */
	public double getValorTotal() {
		return valorTotal;
	}	

	/**
	 * Gets the lista infracciones.
	 *
	 * @return the lista infracciones
	 */
	public IQueue<VOMovingViolations> getListaInfracciones() {
		return listaInfracciones;
	}

	/**
	 * Sets the lista infracciones.
	 *
	 * @param listaInfracciones the new lista infracciones
	 */

	public void añadirListaInfracciones(IQueue<VOMovingViolations>pListaInfracciones) {
		totalInfracciones += pListaInfracciones.size();
		listaInfracciones = new Queue<VOMovingViolations>();
		for(VOMovingViolations violations : pListaInfracciones){
			listaInfracciones.enqueue(violations);
			if(violations.getAccidentIndicator().equalsIgnoreCase("yes"))
				conAccidentes += 1;
			else
				sinAccidentes += 1;
			valorTotal += violations.getTotalPaid();
		}
		porcentajeAccidentes = conAccidentes / (float)(listaInfracciones.size());   //TODO Calcular con base en la lista
		porcentajeNoAccidentes = sinAccidentes / (float)(listaInfracciones.size());
	}

}
